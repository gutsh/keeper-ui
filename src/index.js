import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import './index.css';
//import App from './App';
import * as serviceWorker from './serviceWorker';

const data ={
    _id: 1,
    name: "root",
    subfolders: [
        {
            _id: 2,
            name: "snacks",
            subfolders: [
                {
                    _id: 5,
                    name: "hot snacks",
                    subfolders: [],
                    items: []
                },
                {
                    _id: 6,
                    name: "cold snacks",
                    subfolders: [],
                    items: [
                        {
                            _id: 6,
                            name: "item6"
                        }
                    ]
                }
            ],
            items: [
                {
                    _id: 3,
                    name: "item3"
                }
            ]
        },
        {
            _id: 3,
            name: "soups",
            subfolders: [],
            items: [
                {
                    _id: 4,
                    name: "item4"
                }
            ]
        },
        {
            _id: 4,
            name: "meals",
            subfolders: [
                {
                    _id: 7,
                    name: "steaks",
                    subfolders: [],
                    items: []
                },
                {
                    _id: 8,
                    name: "pastas",
                    subfolders: [
                        {
                            _id: 9,
                            name: "vegeterians",
                            subfolders:[],
                            items: []
                        },
                        {
                            _id: 10,
                            name: "traditional",
                            subfolders: [],
                            items: [
                                {
                                    _id: 7,
                                    name: "item7"
                                }
                            ]
                        }
                    ],
                    items: []
                }
            ],
            items: [
                {
                    _id: 5,
                    name: "item5"
                }
            ]
        }
    ],
    items: [
        {
            _id: 1,
            name: "item1"
        },
        {
            _id: 2,
            name: "item2"
        }
    ]
}

function Order(props){
    let curItems = props.order.map((item, index) => {
        let clicked = {};
        if(index===props.selectedItem){
            clicked.backgroundColor = 'yellow';
        }
        let key = item._id.toString()+"."+index.toString();
        return(
            <li
            key={key}
            onClick={() => props.selectItem(index)}
            style={clicked}
            >
                {item.name}
            </li>
        );
    });
    return(
        <div>
            <ul>
            {curItems}
            </ul>
            <div>
                <button
                onClick={() => props.removeItem(props.selectedItem)}
                >
                    Remove
                </button>
            </div>
        </div>
    );
}

class Menu extends Component{
    constructor(props){
        super(props);
        this.state = {
            path: [
                this.props.data
            ]
        };
    }

    clickFolder(folder){
        let _path = this.state.path.slice();
        _path.push(folder);        
        this.setState({
            path: _path
        });
    }

    moveUp(){
        const upDir = this.state.path.slice(0, this.state.path.length - 1);
        this.setState({
            path: upDir
        })
    }
    
    render(){
        const dots = "...";
        const curDir = this.state.path[this.state.path.length-1];
        let curFolders = curDir.subfolders.map((folder) => {
            return(
                <button
                key={folder._id}
                onClick={() => this.clickFolder(folder)}
                >
                    {folder.name}
                </button>
            );
        });
        let curItems = curDir.items.map(item => {
            return(
                <button
                key={item._id}
                onClick={() => this.props.addItem(item)}
                >
                    {item.name}
                </button>
            );
        });
        if(this.state.path.length > 1){
            curFolders.unshift(
                <button
                key={dots}
                onClick={() => this.moveUp()}
                >
                    {dots}
                </button>
            );
        }
        return(
            <div>
                {curFolders}
                {curItems}
            </div>
        );
    }
}

class WaiterWindow extends Component{
    constructor(props){
        super(props);
        this.state = {
            menu: data,
            order: [],
            selectedItem: null
        };
    }
    
    addItem = item => {
        let newOrder = this.state.order.slice();
        newOrder.push(item);
        this.setState({
            order: newOrder
        });
    }

    selectItem = index => {        
        let toSelect = index;
        if(this.state.selectedItem===index){
            toSelect = null;
        }
        this.setState({
            selectedItem: toSelect
        });
    }

    removeItem = index => {
        if(index==null){
            alert('No item chosen.');
            return;
        }
        let newOrder = this.state.order.slice();
        newOrder.splice(index, 1);
        this.setState({
            order: newOrder,
            selectedItem: null
        });
    }

    render(){
        return(
            <div>
                <Order
                order={this.state.order}
                selectedItem={this.state.selectedItem}
                selectItem={this.selectItem}
                removeItem={this.removeItem}
                />
                <Menu
                data={this.state.menu}
                addItem={this.addItem}
                />
            </div>
        )
    }
}

ReactDOM.render(
  <WaiterWindow />,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
